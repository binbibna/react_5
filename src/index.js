import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { createStore } from "redux";
import App from "./App";
import { rootReducer_Ex_Shoe_Shop_Redux } from "./Ex_Shoe_Shop_Redux/redux/rootReducer_Ex_Shoe_Shop_Redux";
import "./index.css";
import reportWebVitals from "./reportWebVitals";

const store = createStore(
  rootReducer_Ex_Shoe_Shop_Redux,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();