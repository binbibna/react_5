import { type } from "@testing-library/user-event/dist/type";
import { dataShoe } from "../data_shoe";
import * as types from "./constants/shoeContants";
let initialValue = {
  listShoe: dataShoe,
  cart: [],
};

export const shoeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case types.ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }

    case types.DELTETE_ITEM: {
      // splice
      //
      let newCart = state.cart.filter((item) => {
        console.log(action.payload);
        return item.id != action.payload;
      });
      return { ...state, cart: newCart };
    }

    case types.GIAM_ITEM: {
      let cloneCart = [...state.cart];
      cloneCart.forEach((item) => {
        if(Number(item.id) === action.payload){
          item.soLuong--;
          if(item.soLuong <= 0){
            for(var i = 0; i < cloneCart.length; i++){
              if(cloneCart[i].id === action.payload){
                cloneCart.splice(i,1);
              }
            }
          }
        }
      })
        
        return{...state, cart: cloneCart};
    }


    case types.TANG_ITEM: {
      let cloneCart = [...state.cart];
      cloneCart.forEach((item) => {
        if(Number(item.id) === action.payload){
          item.soLuong++;
        }
      })
        
        return{...state, cart: cloneCart};
    }

    default:
      return state;
  }
};
// rootReducer_Ex_Shoe_Shop_Redux
// filter js
